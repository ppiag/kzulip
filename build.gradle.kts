import com.vanniktech.maven.publish.SonatypeHost
import java.net.URI

val logback_version: String by project
val ktor_version: String by project
val kotlin_version: String by project
val VERSION_NAME: String by project

plugins {
    //    application
    kotlin("jvm") version "2.1.10"
    id("com.vanniktech.maven.publish") version "0.31.0"
    id("org.jetbrains.dokka") version "2.0.0"
}

group = "de.ppi.oss"
version = VERSION_NAME

//application {
//    mainClassName = "io.ktor.server.netty.EngineMain"
//}

repositories {
    mavenLocal()
    mavenCentral()
}

kotlin {
    jvmToolchain(21)
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-json:$ktor_version")
    implementation("io.ktor:ktor-client-content-negotiation:$ktor_version")
    implementation("io.ktor:ktor-serialization-jackson:$ktor_version")
    implementation("io.ktor:ktor-client-jackson:$ktor_version")
    implementation("io.ktor:ktor-client-apache:$ktor_version")
    implementation("io.ktor:ktor-client-auth-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-logging-jvm:$ktor_version")
    testImplementation("io.ktor:ktor-client-mock:$ktor_version")
    testImplementation("io.ktor:ktor-client-mock-jvm:$ktor_version")
    testImplementation("junit:junit:4.13.2")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.13")
    testImplementation("ch.qos.logback:logback-classic:$logback_version")
}

dokka {
    moduleName.set("KZulip")
    dokkaSourceSets.main {
        //includes.from("Readme.md")
        sourceLink {
            localDirectory.set(file("src/main/kotlin"))
            remoteUrl.set(URI.create("https://gitlab.com/ppiag/kzulip"))
            remoteLineSuffix.set("#L")
        }
    }
    pluginsConfiguration.html {
//        customStyleSheets.from("styles.css")
//        customAssets.from("logo.png")
        footerMessage.set("(c) Opensource")
    }
}

mavenPublishing {
    publishToMavenCentral(SonatypeHost.DEFAULT)
    signAllPublications()
}
//{
//    nexus {
//        baseUrl = "https://oss.sonatype.org/service/local/"
//        stagingProfile = "de.ppi"
//    }
//}

//kotlin.sourceSets["main"].kotlin.srcDirs("src")
//kotlin.sourceSets["test"].kotlin.srcDirs("test")
//
//sourceSets["main"].resources.srcDirs("resources")
//sourceSets["test"].resources.srcDirs("testresources")
buildscript {
    repositories {
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
    }
    dependencies {
        classpath("com.vanniktech:gradle-maven-publish-plugin:0.13.0")
    }
}

apply(plugin = "com.vanniktech.maven.publish")
apply(plugin = "org.jetbrains.dokka")