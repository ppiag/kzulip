import org.jetbrains.dokka.gradle.DokkaTask

val logback_version: String by project
val ktor_version: String by project
val kotlin_version: String by project
val VERSION_NAME: String by project

plugins {
    //    application
    kotlin("jvm") version "1.4.10"
    id("com.vanniktech.maven.publish") version "0.13.0"
    id("org.jetbrains.dokka") version "0.10.1"
}

group = "de.ppi.oss"
version = VERSION_NAME

//application {
//    mainClassName = "io.ktor.server.netty.EngineMain"
//}

repositories {
    mavenLocal()
    jcenter()
    maven { url = uri("https://kotlin.bintray.com/ktor") }
    maven { url = uri("https://kotlin.bintray.com/kotlinx") }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-json:$ktor_version")
    implementation("io.ktor:ktor-client-jackson:$ktor_version")
    implementation("io.ktor:ktor-client-apache:$ktor_version")
    implementation("io.ktor:ktor-client-auth-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-logging-jvm:$ktor_version")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")
    testImplementation("io.ktor:ktor-client-mock:$ktor_version")
    testImplementation("io.ktor:ktor-client-mock-jvm:$ktor_version")
    testImplementation("junit:junit:4.12")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.13")
    testImplementation("ch.qos.logback:logback-classic:$logback_version")
}

val dokka by tasks.getting(DokkaTask::class) {
    configuration {
        reportUndocumented = false
    }
}

mavenPublish {
    nexus {
        baseUrl = "https://oss.sonatype.org/service/local/"
        stagingProfile = "de.ppi"
    }
}

//kotlin.sourceSets["main"].kotlin.srcDirs("src")
//kotlin.sourceSets["test"].kotlin.srcDirs("test")
//
//sourceSets["main"].resources.srcDirs("resources")
//sourceSets["test"].resources.srcDirs("testresources")
buildscript {
    repositories {
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
    }
    dependencies {
        classpath("com.vanniktech:gradle-maven-publish-plugin:0.13.0")
    }
}

apply(plugin = "com.vanniktech.maven.publish")
apply(plugin = "org.jetbrains.dokka")