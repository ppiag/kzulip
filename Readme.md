# KZulip
Simple library to call the zulip-api.
The easiest way is to instantiate ZulipClient, 
however you can instantiate any io.ktor.client.HttpClientKt.HttpClient.
There is one thing you must ensure: If the status-code >= 300, so an error happens you must throw an exception.
This can be archived on two ways:
* `expectSuccess = false` at HttpClient
* https://ktor.io/clients/http-client/features/response-validation.html. See ZulipClient.

The api calls are implemented as extensions of HttpClient.
They are organized in files with named like the functions.

The documentation is really rare, because I don't want to copy the 
[original documentation](https://zulipchat.com/api/).

You can find the library at [maven central](https://mvnrepository.com/artifact/de.ppi.oss/kzulip).

Version < 1.0 is for zulip 2.x
Current version is for 3.x

## TODO
* Implement further endpoints.
* Make a Java-Client, because the coroutines are difficult to handle from java. 
  See [Stackoverflow](https://stackoverflow.com/questions/52869672/call-kotlin-suspend-function-in-java-class)


