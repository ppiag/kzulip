package de.ppi.oss.kzulip.jackson

import assertk.assertThat
import assertk.assertions.isEqualTo
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import de.ppi.oss.kzulip.api.messages.Recipient
import de.ppi.oss.kzulip.api.messages.ZulipUser
import org.junit.Before
import org.junit.Test
import kotlin.test.assertNull

class RecipientDeserializerTest {

    private val objectMapper = ObjectMapper()

    @Before
    fun configureMapper() {
        objectMapper.registerKotlinModule()
        objectMapper.propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE
        objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
        val module = SimpleModule()
        module.addDeserializer(Recipient::class.java, RecipientDeserializer())
        objectMapper.registerModule(module)
    }

    @Test
    fun deserializeUsers() {
        val json =
            "{\"display_recipient\":[{\"is_mirror_dummy\":false,\"email\":\"opensource21@gmail.com\",\"full_name\":\"Niels\",\"short_name\":\"opensource21\",\"id\":199330},{\"is_mirror_dummy\":false,\"email\":\"test-bot@ctz.zulipchat.com\",\"full_name\":\"test\",\"short_name\":\"test-bot\",\"id\":200436}]}"
        val actual = objectMapper.readValue<Actual>(json)
        val expected = objectMapper.readValue<ExpectedList>(json)
        assertNull(actual.displayRecipient.stream)
        assertThat(actual.displayRecipient.zulipUsers).isEqualTo(expected.displayRecipient)
    }

    @Test
    fun deserializeStream() {
        val json =
            "{\"display_recipient\":\"myStream\"}"
        val actual = objectMapper.readValue<Actual>(json)
        assertNull(actual.displayRecipient.zulipUsers)
        assertThat(actual.displayRecipient.stream).isEqualTo("myStream")
    }


}

data class Actual(
    @JsonProperty("display_recipient")
    val displayRecipient: Recipient
)

data class ExpectedList(
    @JsonProperty("display_recipient")
    val displayRecipient: List<ZulipUser>
)

data class ExpectedStream(
    @JsonProperty("display_recipient")
    val displayRecipient: String
)