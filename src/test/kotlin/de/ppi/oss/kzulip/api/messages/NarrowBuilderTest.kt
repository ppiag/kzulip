package de.ppi.oss.kzulip.api.messages

import assertk.assertions.isEqualTo
import org.junit.Test

class NarrowBuilderTest {

    @Test
    fun stream() {
        assertk.assertThat(NarrowBuilder().stream("Test").build())
            .isEqualTo("[{\"operator\":\"stream\",\"operand\":\"Test\",\"negated\":false}]")
    }

    @Test
    fun topic() {
        assertk.assertThat(NarrowBuilder().stream("Test").topic("ATopic", true).build())
            .isEqualTo("[{\"operator\":\"stream\",\"operand\":\"Test\",\"negated\":false},{\"operator\":\"topic\",\"operand\":\"ATopic\",\"negated\":true}]")

    }

    @Test
    fun id() {
        assertk.assertThat(NarrowBuilder().stream("Test").id(10L).build())
            .isEqualTo("[{\"operator\":\"stream\",\"operand\":\"Test\",\"negated\":false},{\"operator\":\"id\",\"operand\":\"10\",\"negated\":false}]")
    }

    @Test
    fun ist() {
        assertk.assertThat(NarrowBuilder().stream("Test").ist(IsValue.UNREAD, true).build())
            .isEqualTo("[{\"operator\":\"stream\",\"operand\":\"Test\",\"negated\":false},{\"operator\":\"is\",\"operand\":\"unread\",\"negated\":true}]")
    }

    @Test
    fun has() {
        assertk.assertThat(NarrowBuilder().stream("Test").has(HasValue.ATTACHMENT, true).build())
            .isEqualTo("[{\"operator\":\"stream\",\"operand\":\"Test\",\"negated\":false},{\"operator\":\"has\",\"operand\":\"attachment\",\"negated\":true}]")
    }
}