package de.ppi.oss.kzulip.client

import de.ppi.oss.kzulip.api.messages.*
import de.ppi.oss.kzulip.api.streams.GetAllStreamsRequest
import de.ppi.oss.kzulip.api.streams.GetAllStreamsResponse
import de.ppi.oss.kzulip.api.streams.StreamInfo
import de.ppi.oss.kzulip.api.streams.getAllStreams
import de.ppi.oss.kzulip.jackson.ObjectMapperHolder
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.jackson.*

class ZulipClient(
    val site: String,
    val email: String,
    val apikey: String,
    val logLevel: HttpLogLevel = HttpLogLevel.ALL
) {
    private val client = HttpClient(Apache) {
        expectSuccess = false
        install(Auth) {
            basic {
                credentials {
                    BasicAuthCredentials(username = email, password = apikey)
                }
                sendWithoutRequest { true }
            }
        }
        install(Logging) {
            level = LogLevel.valueOf(logLevel.name)
        }
        HttpResponseValidator {
            validateResponse { response ->
                val statusCode = response.status.value
                if (statusCode > 300) throw BadZulipResponseStatusException(
                    response.status,
                    String(response.readRawBytes())
                )
            }
        }
        install(ContentNegotiation) {
            register(ContentType.Application.Json, JacksonConverter(ObjectMapperHolder.objectMapper))
        }
//        install(ResponseObserver) {
//            onResponse({ response ->
//                println(">>>>>")
//                println(response.readText(Charset.forName("UTF-8")))
//                println("<<<<<")
//            })
//        }
        install(UserAgent) { agent = "KZulip/API" }
    }


    suspend fun sendMessage(msg: SendMessageRequest): SendMessageResponse {
        return client.sendMessage(site, msg)
    }

    suspend fun getMessage(msg: GetMessageRequest): GetMessageResponse {
        return client.getMessage(site, msg)
    }

    suspend fun updateMessageFlag(msg: UpdateMessageFlagRequest): UpdateMessageFlagResponse {
        return client.updateMessageFlag(site, msg)
    }

    suspend fun renderMessage(msg: String): GetRenderMessageResponse {
        return client.renderMessage(site, msg)
    }

    suspend fun addReaction(reaction: ReactionRequest): ReactionResponse {
        return client.addReaction(site, reaction)
    }

    suspend fun deleteReaction(reaction: ReactionRequest): ReactionResponse {
        return client.deleteReaction(site, reaction)
    }

    suspend fun getAllStreams(filter: GetAllStreamsRequest): GetAllStreamsResponse {
        return client.getAllStreams(site, filter)
    }

    suspend fun getAllStreams(
        includePublic: Boolean = true,
        includeSubscribed: Boolean = true,
        includeAllActive: Boolean = false,
        includeDefault: Boolean = false,
        includeOwnerSubscribed: Boolean = false
    ): List<StreamInfo> {
        return client.getAllStreams(
            site,
            GetAllStreamsRequest(
                includePublic,
                includeSubscribed,
                includeAllActive,
                includeDefault,
                includeOwnerSubscribed
            )
        ).streams
    }

    suspend fun getStreamInfo(streamId: Long): StreamInfo? {
        return getAllStreams().find { streamInfo: StreamInfo -> streamInfo.streamId == streamId }
    }
}

@Suppress("CanBeParameter", "MemberVisibilityCanBePrivate")
class BadZulipResponseStatusException(
    val statusCode: HttpStatusCode,
    val jsonMessage: String
) : IllegalStateException("Received bad status code: $statusCode. Expected status code < 300. $jsonMessage")






