package de.ppi.oss.kzulip.client

import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import de.ppi.oss.kzulip.api.messages.*
import de.ppi.oss.kzulip.api.streams.GetAllStreamsRequest
import de.ppi.oss.kzulip.api.streams.GetAllStreamsResponse
import de.ppi.oss.kzulip.api.streams.StreamInfo
import de.ppi.oss.kzulip.api.streams.getAllStreams
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.*
import io.ktor.client.features.auth.*
import io.ktor.client.features.auth.providers.*
import io.ktor.client.features.json.*
import io.ktor.client.features.logging.*
import io.ktor.client.statement.*
import io.ktor.http.*

class ZulipClient(
    val site: String,
    val email: String,
    val apikey: String,
    val logLevel: HttpLogLevel = HttpLogLevel.ALL
) {
    private val client = HttpClient(Apache) {
        expectSuccess = false
        install(Auth) {
            basic {
                sendWithoutRequest = true
                username = email
                password = apikey
            }
        }
        install(Logging) {
            level = LogLevel.valueOf(logLevel.name)
        }
        HttpResponseValidator {
            validateResponse { response ->
                val statusCode = response.status.value
                if (statusCode > 300) throw BadZulipResponseStatusException(
                    response.status,
                    // TODO niels 21.08.2019: Hier kann man den Text mit readText nicht lesen.
                    // https://github.com/ktorio/ktor/issues/1038
                    String(response.readBytes())
                )
            }
        }
        install(JsonFeature) {
            serializer = JacksonSerializer {
                propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE
                enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
            }
        }
//        install(ResponseObserver) {
//            onResponse({ response ->
//                println(">>>>>")
//                println(response.readText(Charset.forName("UTF-8")))
//                println("<<<<<")
//            })
//        }
        install(UserAgent) { agent = "KZulip/API" }
    }


    suspend fun sendMessage(msg: SendMessageRequest): SendMessageResponse {
        return client.sendMessage(site, msg)
    }

    suspend fun getMessage(msg: GetMessageRequest): GetMessageResponse {
        return client.getMessage(site, msg)
    }

    suspend fun updateMessageFlag(msg: UpdateMessageFlagRequest): UpdateMessageFlagResponse {
        return client.updateMessageFlag(site, msg)
    }

    suspend fun renderMessage(msg: String): GetRenderMessageResponse {
        return client.renderMessage(site, msg)
    }

    suspend fun addReaction(reaction: ReactionRequest): ReactionResponse {
        return client.addReaction(site, reaction)
    }

    suspend fun deleteReaction(reaction: ReactionRequest): ReactionResponse {
        return client.deleteReaction(site, reaction)
    }

    suspend fun getAllStreams(filter: GetAllStreamsRequest): GetAllStreamsResponse {
        return client.getAllStreams(site, filter)
    }

    suspend fun getAllStreams(
        includePublic: Boolean = true,
        includeSubscribed: Boolean = true,
        includeAllActive: Boolean = false,
        includeDefault: Boolean = false,
        includeOwnerSubscribed: Boolean = false
    ): List<StreamInfo> {
        return client.getAllStreams(
            site,
            GetAllStreamsRequest(
                includePublic,
                includeSubscribed,
                includeAllActive,
                includeDefault,
                includeOwnerSubscribed
            )
        ).streams
    }

    suspend fun getStreamInfo(streamId: Long): StreamInfo? {
        return getAllStreams().find { streamInfo: StreamInfo -> streamInfo.streamId == streamId }
    }
}

@Suppress("CanBeParameter", "MemberVisibilityCanBePrivate")
class BadZulipResponseStatusException(
    val statusCode: HttpStatusCode,
    val jsonMessage: String
) : IllegalStateException("Received bad status code: $statusCode. Expected status code < 300. $jsonMessage")






