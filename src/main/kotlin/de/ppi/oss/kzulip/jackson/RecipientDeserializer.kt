package de.ppi.oss.kzulip.jackson

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import de.ppi.oss.kzulip.api.messages.Recipient
import de.ppi.oss.kzulip.api.messages.ZulipUser
import java.io.IOException

/**
 * Deserialize the "display_recipient"-property, which can be an String or an Array of ZulipUser objects.
 * Both are stored [Recipient].
 */
class RecipientDeserializer @JvmOverloads constructor(vc: Class<*>? = null) : StdDeserializer<Recipient>(vc) {

    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): Recipient {
        val node = jp.codec.readTree<JsonNode>(jp)
        val zulipUserList: List<ZulipUser>?
        val stream: String?
        if (node.isArray) {
            // Very ugly solution, if someone has an better approach he is welcome!
            zulipUserList =
                node.elements().asSequence().map { ObjectMapperHolder.objectMapper.readValue<ZulipUser>(it.toString()) }
                    .toList()
            stream = null
        } else {
            zulipUserList = null
            stream = node.asText()
        }
        return Recipient(stream, zulipUserList)
    }
}

object ObjectMapperHolder {
    val objectMapper = JsonMapper.builder()
    .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
    .propertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE).build().registerKotlinModule()
}
