package de.ppi.oss.kzulip.api.messages

import de.ppi.oss.kzulip.jackson.ObjectMapperHolder

/**
 * Creates a narrow string, for details see [Zulip-Documentation](https://zulipchat.com/help/search-for-messages).
 */
class NarrowBuilder {

    private val objectMapper = ObjectMapperHolder.objectMapper

    private val criterias = ArrayList<Criteria>()

    fun build(): String {
        return objectMapper.writeValueAsString(criterias)
    }

    @JvmOverloads
    fun stream(name: String, negated: Boolean = false): NarrowBuilder {
        criterias.add(Criteria("stream", name, negated))
        return this
    }

    @JvmOverloads
    fun topic(name: String, negated: Boolean = false): NarrowBuilder {
        criterias.add(Criteria("topic", name, negated))
        return this
    }

    /** Just an alias for #topic*/
    @JvmOverloads
    fun subject(name: String, negated: Boolean = false): NarrowBuilder {
        return topic(name, negated)
    }

    @JvmOverloads
    fun pmWith(name: String, negated: Boolean = false): NarrowBuilder {
        criterias.add(Criteria("pm-with", name, negated))
        return this
    }

    @JvmOverloads
    fun groupPmWith(name: String, negated: Boolean = false): NarrowBuilder {
        criterias.add(Criteria("group-pm-with", name, negated))
        return this
    }

    @JvmOverloads
    fun sender(name: String, negated: Boolean = false): NarrowBuilder {
        criterias.add(Criteria("sender", name, negated))
        return this
    }

    @JvmOverloads
    fun near(id: Long, negated: Boolean = false): NarrowBuilder {
        criterias.add(Criteria("near", "$id", negated))
        return this
    }

    @JvmOverloads
    fun id(id: Long, negated: Boolean = false): NarrowBuilder {
        criterias.add(Criteria("id", "$id", negated))
        return this
    }

    @JvmOverloads
    fun ist(value: IsValue, negated: Boolean = false): NarrowBuilder {
        criterias.add(Criteria("is", value.name.lowercase(), negated))
        return this
    }

    @JvmOverloads
    fun has(value: HasValue, negated: Boolean = false): NarrowBuilder {
        criterias.add(Criteria("has", value.name.lowercase(), negated))
        return this
    }

}

private data class Criteria(val operator: String, val operand: String, val negated: Boolean = false)

enum class IsValue {
    PRIVATE, ALERTED, MENTIONED, STARRED, UNREAD
}

enum class HasValue {
    LINK, IMAGE, ATTACHMENT
}