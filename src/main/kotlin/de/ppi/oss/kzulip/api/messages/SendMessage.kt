package de.ppi.oss.kzulip.api.messages

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import de.ppi.oss.kzulip.api.common.CommonResponse
import de.ppi.oss.kzulip.api.common.Result
import de.ppi.oss.kzulip.api.common.ValidationException
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import java.net.URL

data class SendMessageRequest(val to: String, val subject: String?, val content: String, val private: Boolean = false)

const val maxSubjectLength = 60

/** Max length of the content. If it is longer it will be truncated.*/
@Suppress("unused")
const val maxContentSize = 10000

suspend fun HttpClient.sendMessage(site: String, msg: SendMessageRequest): SendMessageResponse {
    if (msg.subject != null && msg.subject.length > maxSubjectLength) {
        throw ValidationException("The subject should have a maximum length of $maxSubjectLength characters. ${msg.subject} has a length of ${msg.subject.length}")
    }

    @Suppress("UnnecessaryVariable")
    val result = this.post<SendMessageResponse> {
        url(URL("$site/api/v1/messages"))
        val type = if (msg.private) {
            "private"
        } else {
            "stream"
        }
        body = FormDataContent(Parameters.build {
            append("to", msg.to)
            if (msg.subject != null) append("subject", msg.subject)
            append("content", msg.content)
            append("type", type)
        })
    }
    return result
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class SendMessageResponse(override val code: String?, override val msg: String, override val result: Result) :
    CommonResponse
