package de.ppi.oss.kzulip.api.common

/**
 * This exception is thrown if the Request is not valid.
 */
class ValidationException(message: String) : Exception(message)