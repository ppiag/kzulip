package de.ppi.oss.kzulip.api.streams

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import de.ppi.oss.kzulip.api.common.CommonResponse
import de.ppi.oss.kzulip.api.common.Result
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.request.url
import io.ktor.http.ContentType
import io.ktor.http.contentType
import java.net.URL


data class GetAllStreamsRequest(
    val includePublic: Boolean = true,
    val includeSubscribed: Boolean = true,
    val includeAllActive: Boolean = false,
    val includeDefault: Boolean = false,
    val includeOwnerSubscribed: Boolean = false
)

suspend fun HttpClient.getAllStreams(site: String, msg: GetAllStreamsRequest): GetAllStreamsResponse {
    val result = this.get<GetAllStreamsResponse> {
        url(URL("$site/api/v1/streams"))
        contentType(ContentType.Application.Json)
        parameter("include_public", msg.includePublic)
        parameter("include_subcribed", msg.includeSubscribed)
        parameter("include_all_active", msg.includeAllActive)
        parameter("include_default", msg.includeDefault)
        parameter("include_owner_subcribed", msg.includeOwnerSubscribed)
    }
    return result
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class GetAllStreamsResponse(
    val streams: List<StreamInfo>,
    override val code: String?, override val msg: String, override val result: Result
) : CommonResponse

@JsonIgnoreProperties(ignoreUnknown = true)
data class StreamInfo(
    val streamId: Long,
    val name: String,
    val description: String,
    val inviteOnly: Boolean
) {
    fun isPrivate(): Boolean {
        return inviteOnly
    }
}
