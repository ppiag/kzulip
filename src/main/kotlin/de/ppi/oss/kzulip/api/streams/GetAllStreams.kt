package de.ppi.oss.kzulip.api.streams

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import de.ppi.oss.kzulip.api.common.CommonResponse
import de.ppi.oss.kzulip.api.common.Result
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import java.net.URI


data class GetAllStreamsRequest(
    val includePublic: Boolean = true,
    val includeSubscribed: Boolean = true,
    val includeAllActive: Boolean = false,
    val includeDefault: Boolean = false,
    val includeOwnerSubscribed: Boolean = false
)

suspend fun HttpClient.getAllStreams(site: String, msg: GetAllStreamsRequest): GetAllStreamsResponse {
    val result = this.get {
        url(URI.create("$site/api/v1/streams").toURL())
        contentType(ContentType.Application.Json)
        parameter("include_public", msg.includePublic)
        parameter("include_subcribed", msg.includeSubscribed)
        parameter("include_all_active", msg.includeAllActive)
        parameter("include_default", msg.includeDefault)
        parameter("include_owner_subcribed", msg.includeOwnerSubscribed)
    }
    return result.body()
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class GetAllStreamsResponse(
    val streams: List<StreamInfo>,
    override val code: String?, override val msg: String, override val result: Result
) : CommonResponse

@JsonIgnoreProperties(ignoreUnknown = true)
data class StreamInfo(
    val streamId: Long,
    val name: String,
    val description: String,
    val inviteOnly: Boolean
) {
    @Suppress("unused")
    fun isPrivate(): Boolean {
        return inviteOnly
    }
}
