package de.ppi.oss.kzulip.api.messages

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import de.ppi.oss.kzulip.api.common.CommonResponse
import de.ppi.oss.kzulip.api.common.Emoji
import de.ppi.oss.kzulip.api.common.Result
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import java.net.URI

data class ReactionRequest(val messageId: Long, val emoji: Emoji)

suspend fun HttpClient.addReaction(site: String, reaction: ReactionRequest): ReactionResponse {
    return this.post {
        url(URI.create("$site/api/v1/messages/${reaction.messageId}/reactions").toURL())
        contentType(ContentType.Application.Json)
        parameter("message_id", reaction.messageId)
        parameter("emoji_name", reaction.emoji.nameInZulip)
    }.body()
}

suspend fun HttpClient.deleteReaction(site: String, reaction: ReactionRequest): ReactionResponse {
    return this.delete {
        url(URI.create("$site/api/v1/messages/${reaction.messageId}/reactions").toURL())
        contentType(ContentType.Application.Json)
        parameter("message_id", reaction.messageId)
        parameter("emoji_name", reaction.emoji.nameInZulip)
    }.body()
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class ReactionResponse(override val code: String?, override val msg: String, override val result: Result) :
    CommonResponse