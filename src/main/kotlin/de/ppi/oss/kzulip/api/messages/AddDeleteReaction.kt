package de.ppi.oss.kzulip.api.messages

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import de.ppi.oss.kzulip.api.common.CommonResponse
import de.ppi.oss.kzulip.api.common.Emoji
import de.ppi.oss.kzulip.api.common.Result
import io.ktor.client.HttpClient
import io.ktor.client.request.delete
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.http.ContentType
import io.ktor.http.contentType
import java.net.URL

data class ReactionRequest(val messageId: Long, val emoji: Emoji)

suspend fun HttpClient.addReaction(site: String, reaction: ReactionRequest): ReactionResponse {
    return this.post<ReactionResponse> {
        url(URL("$site/api/v1/messages/${reaction.messageId}/reactions"))
        contentType(ContentType.Application.Json)
        parameter("message_id", reaction.messageId)
        parameter("emoji_name", reaction.emoji.nameInZulip)
    }
}

suspend fun HttpClient.deleteReaction(site: String, reaction: ReactionRequest): ReactionResponse {
    return this.delete<ReactionResponse> {
        url(URL("$site/api/v1/messages/${reaction.messageId}/reactions"))
        contentType(ContentType.Application.Json)
        parameter("message_id", reaction.messageId)
        parameter("emoji_name", reaction.emoji.nameInZulip)
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class ReactionResponse(override val code: String?, override val msg: String, override val result: Result) :
    CommonResponse