package de.ppi.oss.kzulip.api.common

/**
 * The field which every response contains.
 */
interface CommonResponse {
    /**
     * Sometimes zulip deliveres an specific error-code.
     */
    val code: String?
    /**
     * A message.
     */
    val msg: String
    val result: Result
}

enum class Result {
    SUCCESS, ERROR
}