package de.ppi.oss.kzulip.api.messages

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import de.ppi.oss.kzulip.api.common.CommonResponse
import de.ppi.oss.kzulip.api.common.Flag
import de.ppi.oss.kzulip.api.common.Flag.READ
import de.ppi.oss.kzulip.api.common.Result
import de.ppi.oss.kzulip.api.messages.UpdateMessageFlagOperation.ADD
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import java.net.URI


data class UpdateMessageFlagRequest(
    val messages: List<Long>,
    val op: UpdateMessageFlagOperation = ADD,
    val flag: Flag = READ
)

suspend fun HttpClient.updateMessageFlag(site: String, msg: UpdateMessageFlagRequest): UpdateMessageFlagResponse {
    val result = this.post {
        url(URI.create("$site/api/v1/messages/flags").toURL())
        contentType(ContentType.Application.Json)
        parameter("messages", msg.messages)
        parameter("op", msg.op.name.lowercase())
        parameter("flag", msg.flag.name.lowercase())
    }
    return result.body()
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class UpdateMessageFlagResponse(
    val messages: List<Long>,
    override val code: String?,
    override val msg: String,
    override val result: Result
) : CommonResponse

enum class UpdateMessageFlagOperation { ADD, REMOVE }