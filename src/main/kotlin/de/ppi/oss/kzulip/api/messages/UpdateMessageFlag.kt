package de.ppi.oss.kzulip.api.messages

import de.ppi.oss.kzulip.api.common.CommonResponse
import de.ppi.oss.kzulip.api.common.Flag
import de.ppi.oss.kzulip.api.common.Flag.READ
import de.ppi.oss.kzulip.api.common.Result
import de.ppi.oss.kzulip.api.messages.UpdateMessageFlagOperation.ADD
import io.ktor.client.HttpClient
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.http.ContentType
import io.ktor.http.contentType
import java.net.URL


data class UpdateMessageFlagRequest(
    val messages: List<Long>,
    val op: UpdateMessageFlagOperation = ADD,
    val flag: Flag = READ
)

suspend fun HttpClient.updateMessageFlag(site: String, msg: UpdateMessageFlagRequest): UpdateMessageFlagResponse {
    val result = this.post<UpdateMessageFlagResponse> {
        url(URL("$site/api/v1/messages/flags"))
        contentType(ContentType.Application.Json)
        parameter("messages", msg.messages)
        parameter("op", msg.op.name.toLowerCase())
        parameter("flag", msg.flag.name.toLowerCase())
    }
    return result
}


data class UpdateMessageFlagResponse(
    val messages: List<Long>,
    override val code: String?,
    override val msg: String,
    override val result: Result
) : CommonResponse

enum class UpdateMessageFlagOperation { ADD, REMOVE }