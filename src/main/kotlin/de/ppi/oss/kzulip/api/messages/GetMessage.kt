package de.ppi.oss.kzulip.api.messages

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import de.ppi.oss.kzulip.api.common.CommonResponse
import de.ppi.oss.kzulip.api.common.Result
import de.ppi.oss.kzulip.api.common.ValidationException
import de.ppi.oss.kzulip.jackson.RecipientDeserializer
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import java.net.URI
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime


data class GetMessageRequest(
    val anchor: Long? = null,
    val useFirstUnreadAnchor: Boolean = false,
    val numBefore: Int = 0,
    val numAfter: Int = 1,
    val narrow: String = "[]",
    val clientGravatar: Boolean = false,
    val applyMarkdown: Boolean = false
)

suspend fun HttpClient.getMessage(site: String, msg: GetMessageRequest): GetMessageResponse {
    val anchorAsString = if (msg.anchor == null) {
        if (msg.useFirstUnreadAnchor) {
            "first_unread"
        } else {
            throw ValidationException("if msg.useFirstUnreadAnchor is false, you must define an anchor. $msg")
        }
    } else {
        msg.anchor.toString()
    }

    return this.get {
        url(URI.create("$site/api/v1/messages").toURL())
        contentType(ContentType.Application.Json)
        parameter("anchor", anchorAsString)
        parameter("use_first_unread_anchor", msg.useFirstUnreadAnchor)
        parameter("num_before", msg.numBefore)
        parameter("num_after", msg.numAfter)
        parameter("narrow", msg.narrow)
        parameter("client_gravatar", msg.clientGravatar)
        parameter("apply_markdown", msg.applyMarkdown)
    }.body()
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class GetMessageResponse(
    val anchor: Long,
    val foundNewest: Boolean,
    val foundOldest: Boolean,
    val foundAnchor: Boolean,
    val messages: List<GetMessageDetail>,
    override val code: String?, override val msg: String, override val result: Result
) : CommonResponse

@JsonIgnoreProperties(ignoreUnknown = true)
data class GetMessageDetail(
    val avatarUrl: String?,
    val client: String,
    val content: String,
    val contentType: String,
    @JsonDeserialize(using = RecipientDeserializer::class)
    val displayRecipient: Recipient,
    val flags: List<String>,
    val id: Long,
    val isMeMessage: String,
    val reactions: List<Reaction> = emptyList(),
    val recipientId: Long,
    val senderEmail: String,
    val senderFullName: String,
    val senderId: Long,
    val senderRealmStr: String,
    val streamId: Long?,
    val subject: String?,
    val topicLinks: List<String>,
    val submessages: List<String>,
    val timestamp: Long,
    val type: String
) {
    fun getTopic(): String? {
        return subject
    }

    /**
     * Delivers the time as a ZonedDateTime for the given zone.
     * @param zoneId a specific zone-id or the system-default.
     * @return the time as a ZonedDateTime for the given zone.
     */
    fun getTime(zoneId: ZoneId = ZoneId.systemDefault()): ZonedDateTime {
        return Instant.ofEpochSecond(timestamp).atZone(zoneId)
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class Recipient(val stream: String? = null, val zulipUsers: List<ZulipUser>? = null)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ZulipUser(
    val id: Long,
    val email: String?,
    val fullName: String?,
    val shortName: String?,
    val isMirrorDummy: Boolean
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Reaction(
    val userId: Long,
    val reactionType: String,
    val emojiCode: String,
    val emojiName: String
)