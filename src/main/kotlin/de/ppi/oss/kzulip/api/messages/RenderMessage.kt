package de.ppi.oss.kzulip.api.messages

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import de.ppi.oss.kzulip.api.common.CommonResponse
import de.ppi.oss.kzulip.api.common.Result
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import java.net.URI

suspend fun HttpClient.renderMessage(site: String, msg: String): GetRenderMessageResponse {
    val result = this.post {
        url(URI.create("$site/api/v1/messages/render").toURL())
        setBody(FormDataContent(Parameters.build {
            append("content", msg)
            append("foo", "bar")
        }))
    }

    return result.body<GetRenderMessageResponse>()
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class GetRenderMessageResponse(
    val rendered: String,
    override val code: String?, override val msg: String, override val result: Result
) : CommonResponse